Pod::Spec.new do |s|
  s.name         = "FFmpeg-HIPMO"
  s.version      = "0.0.1"
  s.summary      = "FFmpeg static libraries compiled for iOS, project HIPMO"
  s.homepage     = "https://gitlab.fit.cvut.cz/prusaja3/quanti-ffmpeg-ios"
  
  s.license      = { :type => 'LGPL', :file => 'LICENSE' }
  s.author       = { "Jakub Prusa" => "jakub.prusa@quanti.cz" } # Podspec maintainer
  s.requires_arc = false
  
  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://gitlab.fit.cvut.cz/prusaja3/quanti-ffmpeg-ios.git" }

  s.default_subspec = 'precompiled'

  s.subspec 'precompiled' do |ss|
    ss.source_files        = 'FFmpeg-iOS-binary/include/**/*.h'
    ss.public_header_files = 'FFmpeg-iOS-binary/include/**/*.h'
    ss.header_mappings_dir = 'FFmpeg-iOS-binary/include'
    ss.vendored_libraries  = 'FFmpeg-iOS-binary/lib/*.a'
    ss.libraries = 'avcodec', 'avfilter', 'avformat', 'avutil', 'swresample', 'swscale', 'iconv', 'z', 'bz2'
  end

end
